# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

_Qué cosas necesitas para instalar el software y cómo instalarlas_

```
Se puede ejecutar en Windows, Linux o OS X. Se recomienda crear una máquina virtual (VM) con LINUX para tener un seguimiento y un mantenimiento futuros más.

La máquina virtual recomendable para realizar toda la práctica tendrá un mínimo de 2GHz de
procesador, 4GB de RAM y 25GB de HD. También nos aseguraremos de instalar la última versión estable
de 64 bits de la distribución Ubuntu de Linux (por el momento la 20.04 LTS). 

Las herramientas de las que más nos ayudaremos son: Chrome, Visual Studio Code y Postman. Se pueden descargar desde sus respectivos sitios web.

También podemos aprovechar las herramientas que nos proporciona Ubuntu para la gestión de
instalaciones:
    # Instalamos Visual Studio Code (y lo lanzamos)
    $ sudo snap install --classic code
    $ code .
    # Instalamos Postman (y lo lanzamos)
    $ sudo snap install postman
    $ postman &

```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

**Comenzamos instalando y probando NodeJS**

La instalación de Node JS se puede realizar de muchas formas: desde descargar versión actual (PKG)
desde https://nodejs.org y seguir las instrucciones de instalación; desde utilizar los gestores de paquetes
de Ubuntu (Ubuntu Snap Store ya incluye node), siempre en función del sistema operativo en el que
estemos trabajando; o bien siguiendo alguna de las muchas recetas que encontraremos en la red,
siempre que nos aseguremos que son para el sistema operativo y versión que tenemos, y para la última
versión de Node.

**Instalamos el gestor de repositorios**

Suponiendo que trabajamos bajo Linux, aunque la forma de operar es básicamente la misma en todos los entornos.
Instalamos y configuramos git con nuestros datos de acceso

    $ sudo apt install git

    $ git config --global user.name pmacia

    $ git config --global user.email pmacia@dtic.ua.es

    $ git config --list

    user.name=pmacia

    user.email=pmacia@dtic.ua.es

Si trabajamos en otro entorno, la forma de operar sería básicamente la misma.

**Instalación de Express**

Esta biblioteca proporciona una capa adicional sobre NodeJS que facilita enormemente la gestión de métodos y recursos HTTP.
Se descarga con el comando: 

    $ npm i -S express

**Instalar y ejecutar MongoDB**

Instalamos Mongo de la siguiente forma:

    $ sudo apt update

    $ sudo apt install -y mongodb

Tras la instalación, es posible que nos dejen la base de datos lanzada, en cualquier caso, el inicio (start) y posterior gestión del servicio (status, stop, restart, disable, enable) lo realizaremos mediante el comando systemctl:

    $ sudo systemctl start mongodb 

Finalmente, en otra terminal podemos abrir el gestor de la base de datos (cliente mongo) y probar directamente desde la terminal comandos para gestionarla:

    <Ctrl+Alt+T>

    $ mongo --host 127.0.0.1:27017

    > show dbs

    admin 0.000GB

    config 0.000GB

    local 0.000GB

Finalmente, en nuestro proyecto tendremos que instalar la biblioteca mongodb para trabajar con la base de datos y, en nuestro caso, la biblioteca mongojs que nos simplificará el acceso a mongo desde nuestro proyecto node (en este caso hemos elegido mongojs por su simplicidad, pero es más versátil y conocida la biblioteca mongoose que utilizaremos en la guía de refactorización del código.

    <Ctrl+Alt+T>

    $ cd

    $ cd node/api-rest

    $ npm i -S mongodb

    $ npm i -S mongojs


## Ejecutando las pruebas ⚙️

Para comprobar el funcionamiento de NodeJS y lo sencillo que es poner en marcha un servidor Web bajo esta tecnología. Para ello creamos con nuestro editor de código el archivo index.js y escribimos en él el siguiente código:

    var http = require('http');

    http.createServer( (request, response) => {

    response.writeHead(200, {'Content-Type': 'text/plain'});

    response.end('Hola a todas y a todos!\n’);

    }).listen(8080);

    console.log('Servidor ejecutándose en puerto 8080...');


Para probarlo, desde la terminal ejecutamos el servidor NodeJS con nuestro código index.js.

    $ node index.js

    Servidor ejecutándose en 8080...


Ahora podemos conectarnos desde nuestro navegador web a través del puerto que hemos establecido (http://localhost:8080/) para ver el resultado.


**Nodemon**

Instalamos la biblioteca nodemon mediante la siguiente orden:

    $ npm i -D nodemon #equivale a: npm install —devDependencies nodemon

Podemos iniciar nuestro editor para seguir trabajando con nuestra aplicación.

    $ code .

Utilizando nuestro editor incluimos una línea (resaltada en negrita: "start": "nodemon index.js",) en la
sección scripts del archivo package.json para crear un script de inicio que invoque nodemon

Ahora podemos reiniciar el proyecto de forma que cada vez que hagamos alguna modificación en alguno
de los archivos implicados, automáticamente se actualizará el ejecutable. Por lo tanto, a partir de ahora,
en lugar del tradicional comando node index.js que veníamos ejecutando, lo haremos de la siguiente
forma:

    $ npm start

    > api-rest@1.0.0 start $HOME/node/RESTFulServer

    > nodemon index.js

    [nodemon] 1.17.2

    [nodemon] to restart at any time, enter `rs`

    [nodemon] watching: *.*

    [nodemon] starting `node index.js`

    API REST ejecutándose en http://localhost:3000/hola/:unNombre


Gracias a nodemon, ahora ya no tendremos que detener el anterior servidor, y volver a lanzar la nueva
versión. Cada vez que hagamos un cambio en algunos de los archivos del proyecto, podremos observar a
través de la terminal de texto que el servicio se reinicia automáticamente.

Lo probamos de nuevo con nuestro navegador conectándonos a la URL:  http://localhost:3000/hola/Estudiantes) y el resultado debe ser:
    {"mensaje":"¡Hola Estudiantes desde SD con JSON!"}

### Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Google Chrome](https://www.google.com/intl/es_es/chrome/) - Navegador web
* [Visual Studio Code](https://code.visualstudio.com/) - Editor de código fuente
* [Postman](https://www.postman.com/) - Aplicación que permite realizar pruebas de APIs
* [Express](https://expressjs.com/es/) - Proporciona una capa adicional sobre NodeJS que facilita enormemente la gestión de métodos y recursos HTTP. 
* [NodeJS](https://nodejs.org/es/) - Entorno en tiempo de ejecución multiplataforma para la capa del servidor basado en JavaScript

* [MongoDB](https://www.mongodb.com/es) - Sistema de base de datos NoSQL, orientado a documentos y de código abierto.


## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Bruno García Escudero** - *Trabajo Total*

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
